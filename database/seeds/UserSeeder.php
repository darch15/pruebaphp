<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'David Rodriguez',
            'name_user' => 'davidrch',
            'email' => 'davidrch2013@gmail.com',
            'password' => Hash::make('admin12345'),
            'city' => 'Barranquilla',
            'profile' => 'Administrador',
        ]);
    }
}
