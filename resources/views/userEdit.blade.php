@extends('principal')

@section('content')
    <br><br>
    <div class="panel panel-default">
        <div class="panel-heading"><center><h4>Edit User</h4></center></div>
    </div>
    <div class="panel-body" style="padding-left: 30px; padding-right: 30px">
        <form class="" action="{{ route('home.update', $user->id) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            @if ((auth()->user()->profile == "Administrador" ))
            <div class="form-group">
                <label for="InputName">Name</label>
                <input type="text" name="name" class="form-control" placeholder="Name" value="{{ $user->name }}" required>
            </div>
            <div class="form-group">
                <label for="InputName">User</label>
                <input type="text" name="name_user" class="form-control" placeholder="User" value="{{ $user->name_user }}" required>
            </div>
            <div class="form-group">
                <label for="InputEmail1">Correo</label>
                <input type="email" name="email" class="form-control" placeholder="Correo" value="{{ $user->email }}" required maxlength="100">
            </div>
            <div class="form-group">
                <label for="InputLastName1">City</label>
                <input type="text" name="city" class="form-control" placeholder="City" value="{{ $user->city }}" required>
            </div>
            <div class="form-group">
                <label for="InputLastName2">Profile</label>
                <select name="profile" id="profile" class="form-control">
                    <option {{'Usuario' != $user->profile ? '' : 'selected'}} value="Usuario">Usuario</option>
                    <option {{'Administrador' != $user->profile ? '' : 'selected'}} value="Administrador">Administrador</option>
                </select>
            </div>      
            @endif
            <div class="form-group">
                <textarea class="form-control" required autofocus name="hobby" id="hobby" cols="30" rows="10">{{ $user->hobby }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Actualizar</button>
            <a href="{{ route('home.index') }}" class="btn btn-danger">Cancelar</a>
        </form>
    </div>
@endsection