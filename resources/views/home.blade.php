@extends('principal')

@section('content')
    <br><br>
    <div class="panel panel-default">
        <div class="panel-heading"><center><h4>List Users</h4></center></div>
    </div>
    <table class="table table-striped" style="text-align: center;">
        <thead>
            <th style="text-align: center;">Name</th>
            <th style="text-align: center;">Email</th>
            <th style="text-align: center;">City</th>
            <th style="text-align: center;">Hobby</th>
            <th style="text-align: center;">Edit</th>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td> {{$user->name}} </td>
                <td> {{$user->email}} </td>
                <td> {{$user->city}} </td>
                <td> 
                    <button class="btn btn-info" data-toggle="modal" data-target="#{{'ModalView'.$user->id}}">
                        View
                    </button>
                </td>
                <td>
                    @if ((auth()->user()->id == $user->id or auth()->user()->profile == "Administrador"))
                    <a href="{{ route('home.edit', $user->id) }}" class="btn btn-warning">
                        Edit
                    </a>
                    @endif
                </td>
            </tr>
            @include('Modal')
            @endforeach
        </tbody>
    </table>
@endsection